import librosa
import numpy as np
import os
import soundfile as sf
from random import randint


def pos(lst):
    return [x for x in lst if x > 0] or None


def average(lst):
    return sum(lst) / len(lst)


def normalize(y_m):
    # normalize
    print("normalize")
    peak = max(y_m)
    mul = 0.9 / peak
    y_m *= mul
    print("done")
    return y_m
    
number = 0

path = 'DATASET VOCI CANZONI APPLAUSI'
music_files = os.listdir(path + "/MUSICA")

if not os.path.exists("OUTPUT"):
    os.makedirs("OUTPUT")
while os.path.exists("OUTPUT/list_of_songs_{}".format(number)):
        number += 1
os.makedirs("OUTPUT/list_of_songs_{}".format(number))

n = 0
while n < 15:
    try:
    
        total_l = 0
        total_audio, freq = librosa.load(path + "/MUSICA/"+music_files[randint(0, len(music_files)-1)])
        total_l += total_audio.size/freq
        
        change_list = [0.01]
    
        for i in range(0, 5):
            # leggo musica a caso
            audio, freq = librosa.load(path + "/MUSICA/"+music_files[randint(0, len(music_files)-1)])
            audio = normalize(audio)
            l = audio.size/freq
            total_l += l
            change_list.append(l+change_list[-1])
            total_audio = np.concatenate((total_audio, audio))
            #print(total_audio.shape)
            
        #print(len(change_list))
            
        sf.write("OUTPUT/list_of_songs_{}/".format(number)+str(n)+"_"+"_music.wav", total_audio, freq)
        
        change_size = 0.75
        
        file_segments = open("OUTPUT/list_of_songs_{}/".format(number)+str(n)+"_"+"_music.segments", "a+")
        for c in range (0, len(change_list)-1):
            file_segments.write("{},{},same_song\n".format(change_list[c]+change_size, change_list[c+1]-change_size))
            file_segments.write("{},{},changing_song\n".format(change_list[c+1]-change_size, change_list[c+1]+change_size))
            
        file_segments.write("{},{},same_song\n".format(change_list[-1]+change_size, total_audio.size/freq))
            
        file_segments.close()
        
        #print("done " + str(n))
        n += 1

    except Exception as e:
        print(e)
        print("error, skip")
