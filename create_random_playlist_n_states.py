import os
from random import randint
import wave as w
import librosa
import soundfile as sf
import numpy as np


def pos(lst):
    return [x for x in lst if x > 0] or None


def average(lst):
    return sum(lst) / len(lst)


def normalize(y_m):
    # normalize
    print("normalize")
    peak = max(y_m)
    mul = 0.9 / peak
    y_m *= mul
    print("done")
    return y_m

def remove_segments(path, genres):
    os.chdir(path)
    for i in range(0, len(genres)):
        os.chdir(genres[i])
        os.system("rm *.segments")
        os.chdir("..")
    os.chdir("..")
    return

number = 0

n_states = 3
genres = ["classical", "electronic", "classical"]

path = "genres"

remove_segments(path, genres)

# music_files = os.listdir(path + "classical")
music_files = []
for i in range(0, len(genres)):
    music_files.append(os.listdir(path + "/" + str(genres[i])))
shorter_files = os.listdir("DATASET VOCI CANZONI APPLAUSI/MUSICA")

if not os.path.exists("OUTPUT_" + str(n_states) + "_states"):
    os.makedirs("OUTPUT_" + str(n_states) + "_states")

n = 0
while os.path.exists("OUTPUT_3_states/list_of_songs_{}_mixed_music.wav".format(number)):
        number += 1

n = 0
genre_index = randint(0, len(genres)-1)
total_length = 0
list_of_genres = []
total_audio = []
freq = 0
list_of_songs = [0.01]
while n < 10:
    list_of_genres.append(genre_index)
    if genre_index <= len(genres)-1:
        audio, freq = librosa.load(path + "/" + str(genres[genre_index]) + "/" + music_files[genre_index][randint(0, len(music_files[genre_index]) - 1)])
    else:
        audio, freq = librosa.load("DATASET VOCI CANZONI APPLAUSI/MUSICA/" + shorter_files[randint(0, len(shorter_files) - 1)] )
    l = audio.size / freq
    list_of_songs.append(l + list_of_songs[-1])
    total_audio = np.concatenate((total_audio, audio))
    total_length += l
    genre_index = randint(0,len(genres)*3)
    n = n + 1

sf.write("OUTPUT_" + str(n_states) + "_states/list_of_songs_" + str(number) + "_" + "mixed" + "_music.wav",  total_audio, freq)

file_segments = open(
    "OUTPUT_" + str(n_states) + "_states/list_of_songs_" + str(number) + "_" + "mixed" + "_music.segments", "a+")

change_size = 0.75
# k=0 classic, k=1 elec, k=2 metal
for c in range(0, (len(list_of_songs))-1):
    k = list_of_genres[c]
    if k == 0:
        file_segments.write("{},{},same_song\n".format(list_of_songs[c]+change_size, list_of_songs[c + 1]-change_size))
        if c != len(list_of_songs)-2:
            file_segments.write("{},{},changing_song\n".format(list_of_songs[c+1]-change_size, list_of_songs[c+1]+change_size))
    elif k == 1:
        file_segments.write("{},{},same_song\n".format(list_of_songs[c]+change_size, list_of_songs[c + 1]-change_size))
        if c != len(list_of_songs)-2:
            file_segments.write("{},{},changing_song\n".format(list_of_songs[c+1]-change_size, list_of_songs[c+1]+change_size))
    elif True: #k == 2:
        file_segments.write("{},{},same_song\n".format(list_of_songs[c]+change_size, list_of_songs[c + 1]-change_size))
        if c != len(list_of_songs)-2:
            file_segments.write("{},{},changing_song\n".format(list_of_songs[c+1]-change_size, list_of_songs[c+1]+change_size))

# file_segments.write("{},{},same_song\n".format(change_list[-1]+change_size, total_audio.size/freq))

file_segments.close()
