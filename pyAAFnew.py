# THIS IS THE NEW ONE
from pyAudioAnalysis import audioBasicIO
from pyAudioAnalysis import audioSegmentation as aS
from pyAudioAnalysis import MidTermFeatures as aMTF
import matplotlib.pyplot as plt
import numpy

#aS.train_hmm_from_directory('./OUTPUT/list_of_songs_0', 'hmmChangingSong', 0.3, 0.3)
#print("Training: done!")
#aS.hmm_segmentation('./OUTPUT/list_of_songs_2/7__music.wav', 'hmmChangingSong', True, './OUTPUT/list_of_songs_2/7__music.segments')

[Fs, x] = audioBasicIO.read_audio_file('./OUTPUT/list_of_songs_2/7__music.wav');

midF, _, names = aMTF.mid_feature_extraction(x, Fs, 0.35*Fs, 0.35*Fs, round(Fs * 0.050), round(Fs * 0.050))
for i in range(136):
    plt.plot(midF[i])
    numpy.savetxt('./MidFeaturesPlt/'+names[i]+'.csv', midF[i])
    plt.savefig('./MidFeaturesPlt/'+names[i]+'.png')
    plt.clf()
