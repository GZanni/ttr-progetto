# THIS IS THE NEW ONE
import glob
import numpy as np
from pyAudioAnalysis import audioSegmentation as aS

# Accidentally swapped names, woops
def align_peak(labels_gt, labels, x, same_s):
    c_s = labels_gt[x]
    #print(c_s)
    y = x - 1
    while labels_gt[y] == c_s:
        labels[y] = c_s
        y -= 1
    while labels[y] == c_s and labels_gt[y] == same_s:
        labels[y] = same_s
        y -= 1
    while labels_gt[x] == c_s:
        labels[x] = c_s
        x += 1
    y = x
    x -= 2
    while labels[y] == c_s and labels_gt[y] == same_s:
        labels[y] = same_s
        y += 1
        if y > len(labels_gt) - 1 or y > len(labels_gt) -1:
            break
    return

i = 2
while i < 4:
    print(i)
    #aS.train_hmm_from_directory('./OUTPUT_3_states/Training', 'hmmCSvariant', i, i)
    for j in [0,1]:

        #print("Training: done!")
        #aS.hmm_segmentation('./OUTPUT_3_states/Fitting/list_of_songs_0_mixed_music.wav', 'hmmChangingSong', True, './OUTPUT_3_states/Fitting/list_of_songs_0_mixed_music.segments')

        #Calculate avg.
        total_cm = np.zeros((2,2))
        n = 0

        for s in (glob.glob("./OUTPUT_3_states/Fitting/*.wav")):
            #print(s)
            labels,class_names,temp,c,labels_gt = aS.hmm_segmentation_cm(s, 'hmmChangingSong', True, s.replace('.wav', '.segments'))
            #print(temp)
            
            #print(labels[0]); print(labels_gt[0])

            x = 0
            same_s = labels_gt[0]

            if same_s == j:
                labels_gt = np.where(labels_gt == 0, 2, labels_gt)
                labels_gt = np.where(labels_gt == 1, 0, labels_gt)
                labels_gt = np.where(labels_gt == 2, 1, labels_gt)
                same_s = labels_gt[0]

            #print(labels_gt)
                
            while x < min(len(labels), len(labels_gt)):
                if labels_gt[x] != same_s:
                    while labels_gt[x] != same_s:
                        if labels[x] == labels_gt[x]:
                            align_peak(labels_gt, labels, x, same_s)
                        x += 1
                x += 1

            cm = np.zeros((len(class_names), len(class_names)))
            for index in range(min(len(labels), len(labels_gt))-1):
                cm[int(labels[index]), int(labels_gt[index])] += 1
            #print(cm)
            total_cm = np.add(total_cm, cm)
            n += 1
            #aS.plot_segmentation_results(labels, labels_gt, class_names, 0.4)        
            #print(class_names)

        print("Overall cm:"); print(total_cm)
        #print(np.divide(total_cm,n))
        print(total_cm[0,0])
        same_same = int(total_cm[0,0]); changing_same = int(total_cm[1,0]); same_changing = int(total_cm[0,1]); changing_changing = int(total_cm[1,1]) #prediction_gt
        #new_cm = np.zeros((2,2))
        same_recall = same_same/(same_same + changing_same); same_precision = same_same/(same_same + same_changing)
        changing_recall = changing_changing/(changing_changing + same_changing); changing_precision = changing_changing/(changing_changing + changing_same)
        accuracy = (changing_changing + same_same)/total_cm.sum()
        print("Same recall: " + str(same_recall)); print("Same precision: " + str (same_precision)); 
        print("Changing recall: " + str(changing_recall)); print("Changing precision: " + str(changing_precision)); 
        print("Accuracy: " + str(accuracy))
        print("Total seconds analyzed: " + str(total_cm.sum()*0.3))
        i += 0.025
        if i > 0.5:
            i += 0.025
        
        




        
        
        
        
